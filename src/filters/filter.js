import API from "../api/api.js";

export class Filters {
  makeFilter() {
    API.getAllCards().then(cards => {
      const cardsElem = document.querySelectorAll('.card');
      const filters = this.readFilters();
      const filteredCards = this.filterCards(cards, filters);
      const filteredCardsId = filteredCards.map(item => item.id);
      this.showCard(cardsElem, filteredCardsId);
    })
    }

  readFilters() {
    const filters = {};
    const urgencySelectValue = document.querySelector(".urgency-select").value;
    const doctorSelectValue = document.querySelector(".doctor-select").value;
    const descriptionValue = document.querySelector(".search-string").value;
      if (urgencySelectValue !== 'Urgency') {
          filters.urgency = urgencySelectValue;
      }
      if (doctorSelectValue !== 'Doctors') {
          filters.doctor = doctorSelectValue;
      }
      if (!!descriptionValue){
          filters.description = descriptionValue;
      }

      return filters;
    }

  filterCards(cards, filters) {
    return cards.filter(card => {
      for (const key in filters) {
        if ((key === 'urgency' || key === 'doctor') && filters[key] === 'showAll') continue;
        if (key === 'description') {
          if (!card[key].includes(filters[key])) return false
              continue;
          }
          if (card[key] !== filters[key]) return false
          }
          return true;
    })
    }

    showCard(allCards, filteredCardsId) {
        allCards.forEach(card => {
            if (filteredCardsId.includes(+card.getAttribute('id'))) {
                if (card.style.display !== 'block') card.style.display = 'block';
            } else {
                card.style.display = 'none'
            }
        })
    }

    setUpFilter() {
        const urgencySelectValue = document.querySelector(".urgency-select");
        const doctorSelectValue = document.querySelector(".doctor-select");
        const descriptionValue = document.querySelector(".search-string");
        urgencySelectValue.addEventListener('change',this.makeFilter.bind(this))
        doctorSelectValue.addEventListener('change', this.makeFilter.bind(this))
        descriptionValue.addEventListener('keydown', this.makeFilter.bind(this))
    }
}