import './first_screen/first_screen.css'
import Modal from "./modal/modal.js";
import Form from "./form/form.js";
import './form/form.css'
import './form/formVisit.css'
import './visit/visit.js'
import './visit/visitTherapist.js'
import './visit/visitCardiologist.js'
import './visit/visitDentist.js'
import './Card/card.css'
import './filters/filters.css'
import {Filters} from "./filters/filter.js";


const loginModal = new Modal('login-modal')
const newVisitModal = new Modal('card-modal')

const form = new Form()
const filter = new Filters()
const logInBtn = document.querySelector('#log-in')
const logOutBtn = document.querySelector('#log-out')
const addBtn = document.querySelector('#add-btn')


document.addEventListener('DOMContentLoaded', async () => {
    if (localStorage.getItem('userToken')) {
        logInBtn.classList.remove('active-btn')
        logOutBtn.classList.add('active-btn')
        document.querySelector('#img').style.display = 'none'
        document.querySelector('#add-btn').classList.add('active-btn')
        document.querySelector('.filters').style.display = 'flex'
        await form.checkCards()
        filter.setUpFilter()
    }
})

loginModal.render()
newVisitModal.render()
form.handleClick('login-modal')

logInBtn.addEventListener('click', () => {
    loginModal.appendBody(form.render())
})

logOutBtn.addEventListener('click', () => {
    document.querySelector('#add-btn').classList.remove('active-btn')
    document.querySelector('#img').style.display = 'block'
    document.querySelector('#msg').classList.remove('active-msg')
    document.querySelectorAll('.card').forEach(card => card.remove())
    localStorage.removeItem('userToken')
    document.querySelector('.filters').style.display = 'none'
    toggleBtn()
})

addBtn.addEventListener('click', () => {

    if (document.querySelector('#card-modal .visit-wrapper')) {
        document.querySelector('#card-modal .visit-wrapper').remove()
        const newForm = new Form()
        document.querySelector('.modal').style.display = 'block'
        newForm.visit(document.querySelector('.modal-content'), 'card-modal')
    } else {
        const newForm = new Form()
        document.querySelector('.modal').style.display = 'block'
        newForm.visit(document.querySelector('.modal-content'), 'card-modal')
    }

})

function toggleBtn() {
    logInBtn.classList.toggle('active-btn')
    logOutBtn.classList.toggle('active-btn')
}
