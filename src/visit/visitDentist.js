import Visit from "./visit.js";
import * as Function from '../constans/index.js'

export class VisitDentist extends Visit {
    constructor(
        target,
        description,
        urgency,
        fullName,
        doctor,
        lastVisit, comments) {
        super(target, description, urgency, fullName, doctor, comments);
        this.target = target
        this.description = description
        this.urgency = urgency
        this.fullName = fullName
        this.doctor = doctor
        this.lastVisit = lastVisit
        this.comments = comments
    }

    showDentist(parent) {
        parent.insertAdjacentHTML('afterend', `${Function.makeDentistBlock(this.urgency, this.target, this.description, this.lastVisit, this.comments)}
        `)
    }
}